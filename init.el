;;; package --- Summery
;;; -*- mode: emacs-lisp; coding: utf-8 -*-
;;;
;;; AUTOMATICALLY CREATED FROM my-init.org DON NOT CHANGE
;;;
;;; Date       Name                Why
;;; ========== =================== ====================
;;; 2019-05-04 Anders Jackson      Initial try
;;; 2020-03-31 Anders Jackson      Some cleaning up
;;;
;;; My init file that is loaded when Emacs starts.
;;; It replaces the `~/.emacs.el' (or whatever `user-init-file' is) so
;;; please rename/remove them so this file will be loaded on start.
;;;
;;; To configure Emacs, pleases change `my-init.org'.
;;;
;;; Commentary:
;;;
;;; My `init.el' file.
;;; It is used to bootstrap Emacs and load my `my-init.el'
;;; where all configurations are stored.
;;;
;;; It is set following what Mike Zamansky have done in
;;; his video tutorials.
;;; Great YouTube channel, please have a look at
;;; https://cestlaz.github.io/stories/emacs/
;;; https://www.youtube.com/watch?v=49kBWM3RQQ8
;;; and
;;; https://github.com/zamansky/using-emacs
;;; (Even this: http://emacsblog.org/)

;;;
;;; Code:
;;;

;; Move the Customizing to be saved in another file
;; That is Stuff that is Automatically added by Emacs Custom configuration.
;; See: https://www.emacswiki.org/emacs/CustomizingBoth

(setq custom-file (locate-user-emacs-file "emacs-custom.el"))
(load custom-file 'noerror)

;;
;; Bootstrap Emacs package system, ELPA
;;
;; Set up packages, esp. melpa
;; https://www.emacswiki.org/emacs/ELPA
(require 'package)

;; Bug with GNUTL and Emacs v 26.1
;; Need to turn of TLS 3.1
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=34341#19
(if (version<= emacs-version "26.1")
    (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")))
(add-to-list 'package-archives
	     '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(setq package-enable-at-startup nil)
(package-initialize)

;; Bootstrap use-package
;; Install package
(mapc
 (lambda (package)
   (if (not (package-installed-p package))
       (progn
         (package-refresh-contents)
         (package-install package))))
 '(use-package diminish bind-key))
;; Trigger load of packages
(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)
(setq use-package-always-ensure t)

;; Always use an updated keyring for GNU ELPA.
;; If not, install it.  Need to start Emacs twice to make it work?
;; Update GNU ELPA PGP-certs
;; ;(unless (package-installed-p 'gnu-elpa-keyring-update)
;; ;  (let ((package-check-signature nil))  ;; Don't ever check sig
;; ;    (package-install 'gnu-elpa-keyring-update)
;; ;    (require 'gnu-elpa-keyring-update)))

;; Update packages, if needed
(use-package auto-package-update
  :ensure t
  :config (progn
            (setq auto-package-update-old-version t
                  auto-package-update-hide-results t)
            (auto-package-update-maybe)))

;;
;; Tangel (extract) the elisp code from `my-init.org' into `my-init.el',
;; which then is loaded into my Emacs. Need org-mode.
(use-package org
  :pin gnu
  :ensure t
  :hook ((org-mode . ispell)
         (org-mode . flyspell-mode))
  )

(org-babel-load-file (locate-user-emacs-file "my-init.org"))

(message
 "Loading time for this configuration is %s."
 (emacs-init-time))

;(provide 'init)
;;;
;;; init.el ends here
;;;
